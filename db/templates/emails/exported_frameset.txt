{% extends 'emails/base.txt' %}

{% block main %}
Your download request for frames of satellite with Satellite Identifier "{{ data.sat_id }}" is ready!

{{ data.url }}
{% endblock %}
