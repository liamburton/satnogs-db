# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to add top-level extra dependencies and use
# './contrib/refresh-requirements.sh to regenerate this file
-r requirements.txt

Faker==17.0.0
coverage==7.1.0
distlib==0.3.6
docopt==0.6.1
docopts==0.6.1
exceptiongroup==1.1.0
execnet==1.9.0
factory-boy==3.2.1
filelock==3.9.0
iniconfig==2.0.0
mock==4.0.3
platformdirs==3.0.0
pluggy==1.0.0
py==1.11.0
pytest-celery==0.0.0
pytest-cov==3.0.0
pytest-django==4.5.2
pytest-forked==1.4.0
pytest-xdist==2.5.0
pytest==7.2.1
toml==0.10.2
tomli==2.0.1
tox==3.24.5
virtualenv==20.19.0
